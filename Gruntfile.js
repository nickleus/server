module.exports = function(grunt) {
	require('load-grunt-tasks')(grunt);

	grunt.initConfig({
		coffee: {
      compile: {
        options: {
          bare: true
        },
        files: {
          'index.js': 'dev/index.coffee',
					'lib/app.js': 'dev/lib/app.coffee',
					'lib/bootstrapper.js': 'dev/lib/bootstrapper.coffee',
					'lib/helper.js': 'dev/lib/helper.coffee',
					'lib/helper/logger.js': 'dev/lib/helper/logger.coffee',
					'lib/helper/model.js': 'dev/lib/helper/model.coffee',
					'lib/helper/database.js': 'dev/lib/helper/database.coffee',
					'lib/helper/controller.js': 'dev/lib/helper/controller.coffee',
					'tests/index.js': 'dev/tests/index.coffee',
					'tests/unit/lib/app.js': 'dev/tests/unit/lib/app.coffee',
					'tests/unit/lib/bootstrapper.js': 'dev/tests/unit/lib/bootstrapper.coffee'
        }
      }
    },

		copy: {
			dist: {
				files: [
					{ filter: 'isFile', src: 'dev/config.json', dest: 'config.json' },
					{ filter: 'isFile', src: 'dev/tests/test.json', dest: 'tests/test.json' }
				]
			}
		},

		watch: {
			scripts: {
				files: ['dev/*.coffee', 'dev/*.json', 'dev/**/*.coffee', 'dev/**/*.json', 'dev/**/**/*.coffee', 'dev/**/**/*.json', 'dev/**/**/**/*.coffee', 'dev/**/**/**/*.json'],
				tasks: ['coffee', 'copy'],
				options: {
					spawn: false,
				},
			},
		}
	});

	grunt.registerTask('default', ['watch']);

	grunt.registerTask('tasks', ['coffee', 'copy']);
};
