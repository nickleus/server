### How install the server? ###

Required before the installation :

`sudo npm install coffee-script vows`

ou 

`sudo apt-get install node-vows`

Clone the project in the directory `node_modules` then launch the compilation coffee script like this :

`cd /path/to/module/server && coffee -cbw **/*.coffee **/**/*.coffee`