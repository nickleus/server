'use strict'

module.exports = class Server
  bootstrap: (@bootstrapper) ->
    return @
  run: (callback) ->
    if typeof @bootstrapper is 'undefined'
      bootstrapper = require './lib/bootstrapper'
      @bootstrapper = new bootstrapper()

    @bootstrapper.run()
    @bootstrapper.getApp().run callback
    return
