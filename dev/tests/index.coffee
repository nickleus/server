'use strict'
path = require 'path'
fs = require 'fs'
vows = require 'vows'

options = require './test.json'

includes_dir = options.node['include-dirs']

n_includes_dir = includes_dir.length - 1

suite = vows.describe 'Start unit tests'

load = (files) ->
  n = files.length - 1
  for file in files
    ext = file.substr -3
    if ext is '.js'
      console.log path.resolve dir + '/' + file
      test = require path.resolve dir + '/' + file
      test suite

    if n-- is 0
      if n_includes_dir-- is 0
        suite.run()
        return

for dir in includes_dir
  dir = path.resolve dir
  test_files = fs.readdirSync dir
  load test_files
