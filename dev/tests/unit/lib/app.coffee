'use strict'
assert = require 'assert'
app = require '../../../lib/app'

module.exports = ( ->
  return (vows) ->
    return vows.addBatch
      'lib App':
        'Run should be a function': ->
          i = new app()
          assert.isFunction i.run
          return
    .addBatch
      'lib App - test the callback':
        topic: ->
          i = new app()
          i.run @callback
          return
        'Run should be return object': (e, f) ->
          assert.isObject f
          return
)()
