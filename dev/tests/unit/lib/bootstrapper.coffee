'use strict'
assert = require 'assert'
bootstrapper = require '../../../lib/bootstrapper'

module.exports = ( ->
    return (vows) ->
        return vows.addBatch
            'lib bootstrap':
                'Bootstrap should be a function': ->
                    i = new bootstrapper()
                    assert.isFunction i.initConfig
                    return
                'Run should be a function': ->
                    i = new bootstrapper()
                    assert.isFunction i.run
                    return
)()
