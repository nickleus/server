'use strict'
assert = require 'assert'
index = require '../../index'

config = require '../../config.json'

module.exports = ( ->
  return (vows) ->
    return vows.addBatch
      'Index server - test if functions exists':
        topic: new index()
        'Bootstrap should be a function': (topic) ->
          assert.isFunction topic.bootstrap
          return
        'Run should be a function': (topic) ->
          assert.isFunction topic.run
          return
    .addBatch
      'Index server':
        topic: ->
          i = new index()
          i.run @callback
          return
        'Run should be return an object': (e, f) ->
          assert.isObject f
          return
        'Respond should be equal to 3000': (e, f) ->
          assert.equal f.address().port, config.port
          return
)()
