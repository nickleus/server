'use strict'

path = require 'path'
fs = require 'fs'

module.exports = class Model
  init: ->
    that = @
    @params = arguments[0]
    @app = arguments[1]

    @app.get('event').on 'db error', ->
      console.log "error"
      return

    @app.get('event').on 'db opened', (con) ->
      console.log 'db opened'
      that.loadModels()
      return

    @app.get('logger').info 'Helper loaded', {
      name: @params.name
    }

  loadModels: ->
    models_dir = path.resolve @params.params.path

    files = fs.readdirSync models_dir

    n = files.length - 1

    if n isnt -1
      for file in files
        if file.substr -3 is '.js'
          try
            require path.join(models_dir, file)
          catch e
            @app.get('logger').error 'Load model error', {
              error: e
            }

        if n-- is 0
          @app.get('logger').info 'models loaded'
          @app.get('event').emit('models loaded')
    else
      @app.get('logger').warn 'No models loaded'
