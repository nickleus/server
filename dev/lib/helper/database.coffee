'use strict'

mongoose = require 'mongoose'

module.exports = class Database
  init: ->
    params = arguments[0]
    app = arguments[1]

    uri = 'mongodb://' + params.params.db.host + ':' + params.params.db.port + ':' + params.params.db.database

    con = mongoose.connect uri

    connect = con.connection

    connect.on 'error', ->
      app.get('event').emit('db error')

    connect.once 'open', ->
      app.get('event').emit('db opened', con)

    connect.once 'close', ->
      app.get('event').emit('db closed')

    app.get('logger').info 'Helper loaded', {
      name: params.name
    }
