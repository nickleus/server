'use strict'

winston = require 'winston'
path = require 'path'

module.exports = class Logger
  init: ->
    params = arguments[0]
    app = arguments[1]

    d = new Date()

    curr_date = d.getDate()
    curr_month = d.getMonth()
    curr_year = d.getFullYear()

    date_format = curr_date + '-' + curr_month + '-' + curr_year

    logger = new (winston.Logger)({
      transports: [
        new winston.transports.Console(),
        new winston.transports.File({
          filename: path.resolve params.params.path + 'log-' + date_format + '.log'
        })
      ],
      exceptionHandlers: [
        new winston.transports.File({
          filename: path.resolve params.params.path + 'exceptions-' + date_format + '.log'
        })
      ]
    })

    logger.info 'Helper loaded',  {
      name: params.name
    }

    app.set 'logger', logger
