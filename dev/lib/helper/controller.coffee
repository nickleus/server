'use strict'

path = require 'path'
fs = require 'fs'

module.exports = class Controller
	init: ->
		params = arguments[0]
		app = arguments[1]

		controllers_dir = path.resolve params.params.path

		files = fs.readdirSync controllers_dir
		
		n = files.length - 1
		
		if n isnt -1
			for file in files
				if file.substr -3 is '.js'
					try
						controller = require path.join(controllers_dir, file)
						if typeof controller is 'function'
							controller(app)
					catch
						app.get('logger').error 'Load controller error', {
							error: e	
						}
		app.get('logger').info 'Helper loaded', {
			name: params.name
		}
		return
