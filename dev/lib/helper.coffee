'use strict'
path = require 'path'

module.exports = class Helper
  setOptions: (@helpers) ->

  initHelpers: (app) ->
    for helper_dir in @helpers.path
      dir = path.resolve path.join(__dirname, helper_dir + '/')

      for helper in @helpers.list
        helper_path = path.resolve path.join(dir, helper.name)

        try
          helper_obj = require helper_path 

          obj = new helper_obj()
          obj.init helper, app
        catch e
          console.log e


