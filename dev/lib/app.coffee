'use strict'
express = require 'express'
events = require 'events'

module.exports = class App
  constructor: ->
    @app = express()
    @app.set 'event', new events.EventEmitter()

  set: (key, value) ->
    @app.set key, value
    return @

  get: (key) ->
    return @app.get key

  setHelpers: (app) ->
    helpers = @get 'helpers'
    helpers.initHelpers(app)
    return

  run: (callback) ->
    that_app = @app

    @setHelpers(that_app)

    server = that_app.listen that_app.get('port'), ->
      if typeof callback isnt 'undefined'
        callback(that_app, server)
        return
    return
