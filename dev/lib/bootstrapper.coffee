'use strict'
app = require './app'
helper = require './helper'

config = require '../config.json'

module.exports = class Bootstrapper
  getApp: ->
    return @app

  initConfig: ->
    console.log 'initConfig'
    @config = config
    return

  initApp: ->
    console.log 'initApp'
    @app = new app
    @app.set 'port', @config.port
    return

  initHelper: ->
    console.log 'initHelper'
    @helper = new helper
    @helper.setOptions @config.helpers

    @app.set 'helpers', @helper
    return

  run: (obj) ->
    if typeof obj is 'undefined'
      obj = Bootstrapper

    for prop, i of obj.prototype
      prefix = prop.substr 0, 4
      if prefix is 'init'
        @[prop]()
